//
// Created by Kv on 25.11.2015.
//

#ifndef WIN_API_THREADING_SEMAPHORE_H
#define WIN_API_THREADING_SEMAPHORE_H

#include <windows.h>
#include <string>
#include <map>

class Semaphore {

private:

    static std::map<std::string, Semaphore*> nameToSemaphore;
    HANDLE semaphoreHandle;

    Semaphore(std::string name, int height, int initial);

public:

    static Semaphore* CreateSemaphore(std::string name, int height = 1, int initial = 1);

    bool acquire(DWORD timeout_ms = INFINITE);
    void release();

    ~Semaphore() {
        CloseHandle(semaphoreHandle);
    }

};



#endif //WIN_API_THREADING_SEMAPHORE_H
