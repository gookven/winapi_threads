//
// Created by Kv on 10.12.2015.
//

#ifndef WIN_API_THREADING_CROSSTHREADPIPE_H
#define WIN_API_THREADING_CROSSTHREADPIPE_H

#include <windows.h>
#include <string>
#include "FileView.h"
#include "../common.h"
#include "DataPipe.h"

template< typename T >
class CrossThreadPipe : public DataPipe<T> {

public:
    CrossThreadPipe(std::string fileName) : DataPipe<T>(new FileView(DataPipe<T>::typeSize, fileName)) { }

    virtual ~CrossThreadPipe() {
        delete this->readWritable;
    }

};


#endif //WIN_API_THREADING_CROSSTHREADPIPE_H
