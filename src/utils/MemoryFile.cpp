//
// Created by Kv on 10.12.2015.
//

#include "MemoryFile.h"
#include "../common.h"

MemoryFile::MemoryFile(DWORD bufSize, std::string name) {
    name = applicationPrefix + name;
    memoryFileHandle = CreateFileMapping(
            INVALID_HANDLE_VALUE,    // use paging file
            NULL,                    // default security
            PAGE_READWRITE,          // read/write access
            0,                       // maximum object size (high-order DWORD)
            bufSize,                // maximum object size (low-order DWORD)
            name.c_str()
    );

    if (memoryFileHandle == NULL) {
        ts_printf("Couldn't create memory file");
    }

}

MemoryFile::~MemoryFile() {
    CloseHandle(memoryFileHandle);
}