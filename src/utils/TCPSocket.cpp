//
// Created by Kv on 19.12.2015.
//

#include "TCPSocket.h"

TCPSocket::TCPSocket(SOCKET socket, sockaddr_in address, int addressLength, int retryTimes) {
    this->retryTimes = retryTimes;
    this->address = address;
    this->addressLength = addressLength;
    this->socket = socket;
}

TCPSocket::TCPSocket(int retryTimes) {
    this->retryTimes = retryTimes;
    this->addressLength = sizeof(address);
    socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

sockaddr_in TCPSocket::toAddress(const std::string &ip, int port) {
    memset(&address, 0, sizeof(sockaddr_in));
    address.sin_family = AF_INET;
    if (inet_aton(ip.c_str(), &address.sin_addr) == -1) {
        throw std::runtime_error("Invalid ip: " + ip);
    }
    address.sin_port = htons((u_short) port);

    return address;
}

void TCPSocket::connect(const std::string &ip, int port) {
    address = toAddress(ip, port);
    int tries = 0;
    while (::connect(socket, (sockaddr*)&address, this->addressLength)) {
        tries++;
        if (tries > retryTimes) {
            break;
        }
        ts_printf("Couldn't connect to TCP-socket %s:%d %d times", ip.c_str(), port, tries);
    }

    if (tries > retryTimes) {
        throw std::runtime_error("Couldn't connect to remote socket!");
    }
}

void TCPSocket::listen(const std::string &ip, int port, int que) {
    address = toAddress(ip, port);
    if (bind(socket, (sockaddr*)&address, this->addressLength) < 0) {
        ts_printf("Port %d could be in use already!", port);
        throw std::runtime_error("Couldn't bind socket!");
    }

    if (::listen(socket, que)) {
        throw std::runtime_error("Couldn't start listening!");
    }
}

TCPSocket TCPSocket::accept() {
    sockaddr_in clientSocketAddr;
    int addrLen = sizeof(clientSocketAddr);
    SOCKET clientSocket = ::accept(socket, (sockaddr*)&clientSocketAddr, &addrLen);
    if (clientSocket < 0) {
        throw std::runtime_error("Couldn't accept client's socket, got invalid one!");
    }
    return TCPSocket(clientSocket, clientSocketAddr, addrLen, retryTimes);
}

void TCPSocket::close() {
    closesocket(socket);
}

void TCPSocket::write(void* bytes, DWORD size) {
    DWORD sent = 0;
    DWORD lastSent = 0;

    while (sent < size && lastSent >=0) {
        lastSent = (DWORD) ::send(socket, (char*)(bytes) + sent, (int) (size - sent), 0);
        sent += lastSent;
    }

    if (sent != size) {
        throw std::runtime_error("Error during sending!");
    }

    ts_printf("Sent: %d, Size: %d\n", sent, size);
}

void TCPSocket::read(void* dest, DWORD size) {
    DWORD recieved = 0;
    DWORD lastRecieve = 0;

    if (socket == 0) {
        throw std::runtime_error("Socket is a lie!");
    }

    while (recieved < size && lastRecieve >= 0) {
        lastRecieve = (DWORD) ::recv(socket, (char*)(dest) + recieved, (int) (size - recieved), 0);
        recieved += lastRecieve;
    }

    if (recieved != size) {
        ts_printf("Recieved: %d, Size: %d; Error: %d \n", recieved, size, WSAGetLastError());
        throw std::runtime_error("Error during recieving!");
    }

}