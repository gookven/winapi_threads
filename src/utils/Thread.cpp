//
// Created by Kv on 25.11.2015.
//

#include "Thread.h"
#include "../common.h"

void Thread::start() {
    processHandler = CreateThread(NULL, 0, (DWORD (__attribute__((__stdcall__)) *)(void*))this->runThread, (void*)(new VirtualRunWorkaround(runnable)), 0, NULL );
}

bool Thread::join(DWORD timeout) {
    bool result = WaitForSingleObject(processHandler, timeout) == WAIT_TIMEOUT;
    if (!result) ts_printf("Thread stopped successfully!\n");
    return result;
}

DWORD Thread::runThread(void *pThread) {
    VirtualRunWorkaround *workaround = (VirtualRunWorkaround *)pThread;
    Runnable *r = workaround->runnable;
    delete workaround;
    return r->run();
}