//
// Created by Kv on 19.12.2015.
//

#ifndef WIN_API_THREADING_SOCKET_H
#define WIN_API_THREADING_SOCKET_H


#include <winsock2.h>
#include <string>
#include <stdexcept>
#include "../aton.h"
#include "ReadWritable.h"
#include "../common.h"

class TCPSocket : public ReadWritable {

private:
    SOCKET socket;
    sockaddr_in address;
    int addressLength;
    int retryTimes;

    TCPSocket(SOCKET socket, sockaddr_in address, int addressLength, int retryTimes = 0);

public:
    TCPSocket(int retryTimes = 0);

    ~TCPSocket() { }

    sockaddr_in toAddress(const std::string &ip, int port);

    void connect(const std::string &ip = "127.0.0.1", int port = 5454);

    void listen(const std::string &ip = "0.0.0.0", int port = 5454, int que = 1);

    TCPSocket accept();

    void close();

    virtual void write(void* bytes, DWORD size);

    virtual void read(void* dest, DWORD size);

};


#endif //WIN_API_THREADING_SOCKET_H
