//
// Created by Kv on 10.12.2015.
//

#ifndef WIN_API_THREADING_FILEVIEW_H
#define WIN_API_THREADING_FILEVIEW_H

#include <windows.h>
#include <string>
#include "ReadWritable.h"

class FileView : public ReadWritable {

private:

    HANDLE fileViewHandle;
    LPCTSTR buffer;

public:

    FileView(DWORD bufferSize, std::string name);
    ~FileView();

    virtual void write(void* bytes, DWORD size) override {
        CopyMemory((PVOID)buffer, bytes, size);
    }

    virtual void read(void* dest, DWORD size) override {
        CopyMemory(dest, (PVOID)buffer, size);
    }

};


#endif //WIN_API_THREADING_FILEVIEW_H
