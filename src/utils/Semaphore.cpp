//
// Created by Kv on 25.11.2015.
//

#include <iostream>
#include "Semaphore.h"
#include "../common.h"

Semaphore::Semaphore(std::string name, int height, int initial) {

    semaphoreHandle = OpenSemaphore(SEMAPHORE_ALL_ACCESS, true, name.c_str());

    if (semaphoreHandle == NULL) {
        semaphoreHandle = ::CreateSemaphore(NULL, initial, height, name.c_str());
        if (semaphoreHandle == NULL) {
            ts_printf("Couldn't create semaphore!\n");
        }
    }

}

Semaphore* Semaphore::CreateSemaphore(std::string name, int height, int initial) {
    return new Semaphore(name, height, initial);
}

bool Semaphore::acquire(DWORD timeout_ms) {
    return WaitForSingleObject(semaphoreHandle, timeout_ms) == WAIT_TIMEOUT;
}

void Semaphore::release() {
    ReleaseSemaphore(semaphoreHandle, 1, NULL);
}
