//
// Created by Kv on 25.11.2015.
//

#ifndef WIN_API_THREADING_THREAD_H
#define WIN_API_THREADING_THREAD_H

#include <windows.h>
#include <iostream>
#include "Runnable.h"

class Thread {

private:

    static DWORD runThread(void *pThread);
    HANDLE processHandler;
    Runnable *runnable;

public:

    Thread(Runnable *r) {
        runnable = r;
    }

    virtual void start();
    virtual bool join(DWORD timeout = INFINITE);

};

struct VirtualRunWorkaround {
    VirtualRunWorkaround(Runnable* r) : runnable(r) {};
    Runnable* runnable;
};


#endif //WIN_API_THREADING_THREAD_H
