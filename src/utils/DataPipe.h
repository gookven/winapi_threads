//
// Created by Kv on 18.12.2015.
//

#ifndef WIN_API_THREADING_DATAPIPE_H
#define WIN_API_THREADING_DATAPIPE_H

#include <type_traits>
#include "ReadWritable.h"

template <typename T>
class DataPipe {
// only POD types allowed!
static_assert(std::is_pod<T>::value, "T must be POD");

protected:
    ReadWritable *readWritable;
    static const DWORD typeSize = sizeof(T);

    void setReadWritable(ReadWritable *rw) {
        readWritable = rw;
    }

public:

    DataPipe() { }

    DataPipe(ReadWritable *rw) {
        setReadWritable(rw);
    }

    virtual ~DataPipe() {}

    virtual void write(T &item) {
        readWritable->write(&item, DataPipe<T>::typeSize);
    }

    virtual T read() {
        T buffer;
        readWritable->read(&buffer, DataPipe<T>::typeSize);
        return buffer;
    }

};


#endif //WIN_API_THREADING_DATAPIPE_H
