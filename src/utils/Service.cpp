//
// Created by Kv on 24.11.2015.
//

#include <iostream>
#include "Service.h"

DWORD Service::run() {
    while (isStarted) {
        DWORD returnCode = execute();
        if (returnCode) {
            std::cerr << "Thread stopped due to execution error; Error Code - " << returnCode << std::endl;
            return returnCode;
        }
    }
    return 0;
}

Service::Service(bool startInstantly) {
    initialize();
    if (startInstantly) {
        start();
    }
}

bool Service::isRunning() {
    return isStarted;
}

void Service::start() {
    isStarted = true;
}

void Service::stop() {
    isStarted = false;
}

