//
// Created by Kv on 24.11.2015.
//

#ifndef WIN_API_THREADING_SERVICE_H
#define WIN_API_THREADING_SERVICE_H

#include <windows.h>
#include "Thread.h"

class Service : public Runnable {

private:

    bool isStarted;
    DWORD run() override;

public:

    Service(bool startInstantly = false);

    bool isRunning();

    virtual void start();
    virtual void stop();

protected:

    virtual void initialize() {  };
    virtual DWORD execute() { return 0; };

};


#endif //WIN_API_THREADING_SERVICE_H
