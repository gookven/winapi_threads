//
// Created by Kv on 18.12.2015.
//

#ifndef WIN_API_THREADING_READWRITABLE_H
#define WIN_API_THREADING_READWRITABLE_H


#include <windef.h>

class ReadWritable {

public:
    virtual void write(void* bytes, DWORD size) = 0;
    virtual void read(void* dest, DWORD size) = 0;

};


#endif //WIN_API_THREADING_READWRITABLE_H
