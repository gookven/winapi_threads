//
// Created by Kv on 10.12.2015.
//

#ifndef WIN_API_THREADING_ONEWAYPIPE_H
#define WIN_API_THREADING_ONEWAYPIPE_H

#include <exception>
#include "CrossThreadPipe.h"
#include "Semaphore.h"
#include "../common.h"

template <typename T>
class OneWayPipe : public CrossThreadPipe<T> {

    Semaphore *bufferFullSemaphore;
    Semaphore *bufferEmptySemaphore;

public:

    OneWayPipe(const std::string &fileName) : CrossThreadPipe<T>(fileName) {
        string name = applicationPrefix + fileName;
        bufferFullSemaphore = Semaphore::CreateSemaphore(name + "/full", 1, 0);
        bufferEmptySemaphore = Semaphore::CreateSemaphore(name + "/empty");
    }

    virtual ~OneWayPipe() {
        delete bufferFullSemaphore;
        delete bufferEmptySemaphore;
    }

    virtual void write(T item, DWORD timeout_ms = INFINITE) {
        if (bufferEmptySemaphore->acquire(timeout_ms)) {
            throw std::runtime_error("Write semaphore timeout exceeded!");
        }
        CrossThreadPipe<T>::write(item);
        bufferFullSemaphore->release();
    }

    virtual T read(DWORD timeout_ms = INFINITE) {
        if (bufferFullSemaphore->acquire(timeout_ms)) {
            throw std::runtime_error("Read semaphore timeout exceeded!");
        }
        T result = CrossThreadPipe<T>::read();
        bufferEmptySemaphore->release();
        return result;
    }

};


#endif //WIN_API_THREADING_ONEWAYPIPE_H
