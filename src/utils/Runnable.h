//
// Created by Kv on 17.12.2015.
//

#ifndef WIN_API_THREADING_RUNNABLE_H
#define WIN_API_THREADING_RUNNABLE_H


#include <windows.h>

class Runnable {
public:
    virtual DWORD run() = 0;
};


#endif //WIN_API_THREADING_RUNNABLE_H
