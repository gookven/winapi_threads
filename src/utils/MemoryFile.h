//
// Created by Kv on 10.12.2015.
//

#ifndef WIN_API_THREADING_FILEMAPPING_H
#define WIN_API_THREADING_FILEMAPPING_H


#include <windows.h>
#include <string>

class MemoryFile {

private:

    HANDLE memoryFileHandle;

public:

    MemoryFile(DWORD bufSize, std::string name);
    ~MemoryFile();

};


#endif //WIN_API_THREADING_FILEMAPPING_H
