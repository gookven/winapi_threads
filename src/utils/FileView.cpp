//
// Created by Kv on 10.12.2015.
//

#include "FileView.h"
#include "../common.h"

FileView::FileView(DWORD bufferSize, std::string name) {
    name = applicationPrefix + name;

    fileViewHandle = CreateFileMapping(
            INVALID_HANDLE_VALUE,    // use paging file
            NULL,                    // default security
            PAGE_READWRITE,          // read/write access
            0,                       // maximum object size (high-order DWORD)
            bufferSize,              // maximum object size (low-order DWORD)
            name.c_str()             // name of mapping object
    );

    if (fileViewHandle == NULL) {
        ts_printf("Couldn't create FileView");
    }

    buffer = (LPTSTR) MapViewOfFile(
            fileViewHandle, // handle to map object
            FILE_MAP_ALL_ACCESS,  // read/write permission
            0,
            0,
            bufferSize
    );
}

FileView::~FileView() {
    UnmapViewOfFile(buffer);
    CloseHandle(fileViewHandle);
}