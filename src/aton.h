//
// Created by Kv on 19.12.2015.
//

#ifndef WIN_API_THREADING_ATON_H
#define WIN_API_THREADING_ATON_H

#include <winsock2.h>

#ifdef WIN32
static int inet_aton(const char *cp, struct in_addr *inp)
{
    if (cp == 0 || inp == 0)
    {
        return -1;
    }

    unsigned long addr = inet_addr(cp);
    if (addr == INADDR_NONE)// || addr == INADDR_ANY)
    {
        return -1;
    }

    inp->s_addr = addr;

    return 0;
}
#endif

#endif //WIN_API_THREADING_ATON_H
