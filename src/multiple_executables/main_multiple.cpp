//
// Created by Kv on 17.12.2015.
//

#include <conio.h>
#include "../utils/MemoryFile.h"
#include "../common.h"

int main() {
    MemoryFile *filebtu = new MemoryFile(sizeof(int), PipeName::userToBrowser);
    MemoryFile *fileutb = new MemoryFile(sizeof(char), PipeName::browserToUser);
    MemoryFile *filebtw = new MemoryFile(sizeof(Request), PipeName::browserToWebserver);
    MemoryFile *filewtb = new MemoryFile(sizeof(Response), PipeName::webserverToBrowser);

    MemoryFile *filebtp = new MemoryFile(sizeof(Response), PipeName::browserToParser);
    MemoryFile *fileptbr = new MemoryFile(sizeof(int), PipeName::parserToBrowserResources);
    MemoryFile *fileptbd = new MemoryFile(sizeof(ParsedWebpageData), PipeName::parserToBrowserData);

    Sleep(ExecutionTime);

    delete filebtu;
    delete fileutb;
    delete filebtw;
    delete filewtb;

    delete filebtp;
    delete fileptbr;
    delete fileptbd;

    ts_printf("\n\n STOPPED! Press any key to exit!");
    getch();

    return 0;
}
