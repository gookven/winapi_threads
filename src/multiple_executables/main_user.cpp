//
// Created by Kv on 17.12.2015.
//

#include <conio.h>
#include "../common.h"
#include "../browsing/User.h"

int main() {
    User user;
    user.start();
    Thread t(&user);
    t.start();

    Sleep(ExecutionTime);

    user.stop();
    t.join();

    ts_printf("\n\n STOPPED! Press any key to exit!");
    getch();

    return 0;
}
