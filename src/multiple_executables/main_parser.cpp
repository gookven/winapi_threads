//
// Created by Kv on 17.12.2015.
//

#include <conio.h>
#include "../common.h"
#include "../browsing/Parser.h"

int main() {
    Parser parser;
    parser.start();
    Thread t(&parser);
    t.start();

    Sleep(ExecutionTime);

    parser.stop();
    t.join();

    ts_printf("\n\n STOPPED! Press any key to exit!");
    getch();

    return 0;
}