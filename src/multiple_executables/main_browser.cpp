//
// Created by Kv on 17.12.2015.
//

#include <conio.h>
#include "../common.h"
#include "../browsing/Browser.h"

int main() {
    WSADATA wsd;
    if (WSAStartup (MAKEWORD(2, 2), &wsd) != 0) //инициализация
    {
        ts_printf("WSAStartup failed\n");
        return 1;
    }


    Browser browser;
    browser.start();
    Thread t(&browser);
    t.start();

    Sleep(ExecutionTime);

    browser.stop();
    t.join();

    ts_printf("\n\n STOPPED! Press any key to exit!");
    getch();

    WSACleanup();

    return 0;
}