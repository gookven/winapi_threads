//
// Created by Kv on 17.12.2015.
//


#include <conio.h>
#include "../common.h"
#include "../browsing/WebServer.h"

int main() {
    WSADATA wsd;
    if (WSAStartup (MAKEWORD(2, 2), &wsd) != 0) //инициализация
    {
        ts_printf("WSAStartup failed\n");
        return 1;
    }


    std::set<char> servedWebsites;
    servedWebsites.insert('a');
    servedWebsites.insert('b');
    servedWebsites.insert('c');
    servedWebsites.insert('d');
    servedWebsites.insert('f');

    WebServer webServer(servedWebsites);
    webServer.start();
    Thread t(&webServer);
    t.start();

    Sleep(ExecutionTime);

    webServer.stop();
    t.join();

    ts_printf("\n\n STOPPED! Press any key to exit!");
    getch();

    WSACleanup();

    return 0;
}