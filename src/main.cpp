#include <iostream>
#include <vector>

#include "common.h"

#include "utils/Service.h"
#include "utils/MemoryFile.h"
#include "utils/OneWayPipe.h"
#include "browsing/User.h"
#include "browsing/Browser.h"
#include "browsing/WebServer.h"
#include "browsing/Parser.h"

using namespace std;

int main() {
    WSADATA wsd;
    if (WSAStartup (MAKEWORD(2, 2), &wsd) != 0) //инициализация
    {
        ts_printf("WSAStartup failed\n");
        return 1;
    }

    MemoryFile *filebtu = new MemoryFile(sizeof(int), PipeName::userToBrowser);
    MemoryFile *fileutb = new MemoryFile(sizeof(char), PipeName::browserToUser);
    MemoryFile *filebtw = new MemoryFile(sizeof(Request), PipeName::browserToWebserver);
    MemoryFile *filewtb = new MemoryFile(sizeof(Response), PipeName::webserverToBrowser);

    MemoryFile *filebtp = new MemoryFile(sizeof(Response), PipeName::browserToParser);
    MemoryFile *fileptbr = new MemoryFile(sizeof(int), PipeName::parserToBrowserResources);
    MemoryFile *fileptbd = new MemoryFile(sizeof(ParsedWebpageData), PipeName::parserToBrowserData);

    std::set<char> servedWebsites;
    servedWebsites.insert('a');
    servedWebsites.insert('b');
    servedWebsites.insert('c');
    servedWebsites.insert('d');
    servedWebsites.insert('f');

    User user;
    Browser browser;
    WebServer webServer(servedWebsites);
    Parser parser;

    std::vector<Service*> services;
    services.push_back(&user);
    services.push_back(&browser);
    services.push_back(&webServer);
    services.push_back(&parser);

    std::vector<Thread> threads;

    for (auto i = services.begin(); i != services.end(); i++) {
        Service *service = *i;
        service->start();
        Thread t = Thread(service);
        threads.push_back(t);
        t.start();
    }

    Sleep(ExecutionTime);

    for (auto i = services.begin(); i != services.end(); i++) {
        (*i)->stop();
    }
    for (auto i = threads.begin(); i != threads.end(); i++) {
        (*i).join();
    }

    delete filebtu;
    delete fileutb;
    delete filebtw;
    delete filewtb;

    delete filebtp;
    delete fileptbr;
    delete fileptbd;

    WSACleanup();

    return 0;
}