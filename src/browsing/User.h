//
// Created by Kv on 11.12.2015.
//

#ifndef WIN_API_THREADING_USER_H
#define WIN_API_THREADING_USER_H

#include "../common.h"
#include "../utils/Service.h"
#include "../utils/OneWayPipe.h"

class User : public Service {

    OneWayPipe<char> *userToBrowserPipe;
    OneWayPipe<int> *browserToUserPipe;

public:
    User() : Service() {
        initialize();
    }

    ~User() {
        delete userToBrowserPipe;
        delete browserToUserPipe;
    }

protected:

    virtual void initialize() override {
        userToBrowserPipe = new OneWayPipe<char>(PipeName::userToBrowser);
        browserToUserPipe = new OneWayPipe<int>(PipeName::browserToUser);
    }

    DWORD virtual execute() override {

        // site name
        char site = (char) ('a' + rand() % 20);

        // let's make request
        try {
            ts_printf("User types in request: %c \n", site);
            userToBrowserPipe->write(site, 2000);
        } catch (runtime_error exception) {
            ts_printf(
                    "The user couldn't type his request during 2s, looks like browser is freezed! \n Error: %s \n",
                exception.what()
            );

            return 1;
        }

        // so we typed our request and pressed enter
        // waiting for the browser to answer
        int resultCode;
        try {
            resultCode = browserToUserPipe->read(5000);
        } catch (runtime_error exception) {
            ts_printf(
                    "The user has been waiting for 5s, but webpage wasn't displayed! \n Error: %s \n",
                    exception.what()
            );

            return 1;
        }

        ts_printf("User recieved page! \n");

        if (resultCode >= 200 && resultCode < 300) {
            ts_printf("User sees correct webpage! \n");
        } else {
            ts_printf("User sees error-page! Result code: %d \n", resultCode);
        }

        Sleep(1000);

        return 0;
    }

};


#endif //WIN_API_THREADING_USER_H
