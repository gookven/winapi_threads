//
// Created by Kv on 16.12.2015.
//

#ifndef WIN_API_THREADING_PARSER_H
#define WIN_API_THREADING_PARSER_H

#include <set>
#include <stdio.h>
#include "../utils/Service.h"
#include "../utils/OneWayPipe.h"

class Parser : public Service {

    OneWayPipe<Response> *browserToParserPipe;
    OneWayPipe<int> *parserToBrowserResources;
    OneWayPipe<ParsedWebpageData> *parserToBrowserData;
    Semaphore *isParsingSemaphore;
public:
    Parser() : Service() {
        initialize();
    }

    ~Parser() {
        delete browserToParserPipe;
        delete parserToBrowserResources;
        delete isParsingSemaphore;
    }

protected:

    virtual void initialize() override {
        browserToParserPipe = new OneWayPipe<Response>(PipeName::browserToParser);
        parserToBrowserResources = new OneWayPipe<int>(PipeName::parserToBrowserResources);
        parserToBrowserData = new OneWayPipe<ParsedWebpageData>(PipeName::parserToBrowserData);
        isParsingSemaphore = Semaphore::CreateSemaphore("parser/working");
    }

    DWORD virtual execute() override {

        ts_printf("Parser is wating for strings to parse! \n");

        if (isParsingSemaphore->acquire(2000)) {
            ts_printf(
                    "Parser couldn't acquire it's work-semaphore in 2s! \n"
            );

            return 0;
        }

        Response toParse;

        try {
            toParse = browserToParserPipe->read(2000);
            ts_printf("Parser got data for parsing: %s \n", toParse.plainTextResponse);
        } catch (runtime_error exception) {
            ts_printf(
                    "No requests to parser within 2s! \n Error: %s \n",
                    exception.what()
            );

            isParsingSemaphore->release();
            return 0;
        }

        ts_printf("Parsing response! \n");

        ParsedWebpageData data;
        data.linksCount = 0;
        data.resourcesCount = 0;
        data.isCorrect = true;

        if (toParse.resultCode >= 200 && toParse.resultCode < 300) {
            for (int i = 0; i < toParse.length; i+=2) {
                char next = toParse.plainTextResponse[i+1];
                switch (toParse.plainTextResponse[i]) {
                    case 'R': {
                        ts_printf("Parsed resource mention: %c \n", next);
                        data.resources[data.resourcesCount++] = next - '0';

                        // trying to send found resource info
                        bool isSend = false;
                        while (!isSend) {

                            try {
                                parserToBrowserResources->write(next - '0', 200);
                                isSend = true;
                            } catch (runtime_error exception) {
                                ts_printf("Couldn't send found resource info in 200ms! Retry... \n");
                            }

                        }

                    }; break;
                    case 'L': {
                        ts_printf("Parsed link: %c \n", next);
                        data.links[data.linksCount++] = next;
                    }; break;
                    default: {
                        ts_printf("Parse error!\n");
                        data.isCorrect = false;
                    }
                }
                Sleep(100);
            }
        }

        isParsingSemaphore->release();

        // sending parsed data to browser
        try {
            parserToBrowserData->write(data, 2000);
        } catch (runtime_error exception) {
            ts_printf(
                    "Couldn't send parsed data to browser within 2s! \n Error: %s \n",
                    exception.what()
            );

            return 0;
        }


        return 0;
    }

};

#endif //WIN_API_THREADING_PARSER_H
