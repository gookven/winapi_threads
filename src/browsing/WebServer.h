//
// Created by Kv on 15.12.2015.
//

#ifndef WIN_API_THREADING_WEBSERVER_H
#define WIN_API_THREADING_WEBSERVER_H

#include <set>
#include <stdio.h>
#include <vector>
#include "../utils/Service.h"
#include "../utils/OneWayPipe.h"
#include "../utils/TCPSocket.h"

class WebServerRoutine : public Runnable {

private:
    TCPSocket clientSocket;
    set<char> servedSites;

public:

    WebServerRoutine(set<char> &servedSites, TCPSocket clientSocket) {
        this->servedSites = servedSites;
        this->clientSocket = clientSocket;
    }

    virtual DWORD run() override {
        Request request;

        try {
            DataPipe<Request> requestThroughSocket(&clientSocket);
            request = requestThroughSocket.read();
            ts_printf("Webserver got request: %c \n", request.site);
        } catch (runtime_error exception) {
            ts_printf(
                    "During recieve! \n Error: %s \n",
                    exception.what()
            );

            clientSocket.close();

            return 0;
        }

        Response response;

        if (servedSites.count(request.site)) {
            response.resultCode = 200;

            ts_printf("The request is correct! Creating response!\n");

            srand((unsigned int)request.site);
            response.length = (rand() % 8 + 1) * 2;

            for (int i = 0; i < response.length; i+= 2) {
                if (rand() % 2) {
                    // add resource
                    response.plainTextResponse[i] = 'R';
                    response.plainTextResponse[i+1] = (char) ('0' + (rand() % 10));
                } else {
                    // add link
                    response.plainTextResponse[i] = 'L';
                    response.plainTextResponse[i+1] = (char) ('a' + (rand() % 16));
                }
            }

            response.plainTextResponse[response.length] = 0;

        } else {
            ts_printf("Invalid request! 404: No page '%c' served!\n", request.site);
            response.length = 0;
            response.plainTextResponse[0] = 0;
            response.resultCode = 404;
        }

        try {
            DataPipe<Response> responseThroughSocket(&clientSocket);
            responseThroughSocket.write(response);
        } catch (runtime_error exception) {
            ts_printf(
                    "Webserver couldn't send response in 5s! Timeout exceeded! \n Error: %s \n",
                    exception.what()
            );

            clientSocket.close();

            return 0;
        }

        clientSocket.close();

        return 0;
    }

};

class WebServer : public Service {

    OneWayPipe<Request> *browserToWebserverRequestPipe;
    OneWayPipe<Response> *webserverToBrowserPipe;
    std::set<char> servedSites;

    std::vector<std::pair<Thread*, Runnable*> > routines;

    TCPSocket serverSocket;

public:
    WebServer() : Service() {
        initialize();
    }

    WebServer(std::set<char> &toServe) : Service() {
        servedSites = toServe;
        initialize();
    }

    ~WebServer() {
        serverSocket.close();
        delete browserToWebserverRequestPipe;
        delete webserverToBrowserPipe;
    }

protected:

    virtual void initialize() override {
        browserToWebserverRequestPipe = new OneWayPipe<Request>(PipeName::browserToWebserver);
        webserverToBrowserPipe = new OneWayPipe<Response>(PipeName::webserverToBrowser);
        serverSocket.listen();
    }

    DWORD virtual execute() override {

        ts_printf("Webserver is waiting for requests!\n");

        TCPSocket clientSocket;

        try {
            clientSocket = serverSocket.accept();
        } catch (runtime_error exception) {
            ts_printf(
                    "Couldn't connect to client! \n Error: %s \n",
                    exception.what()
            );

            return 0;
        }

        // we have client's socket, let the other routine work with that,
        Runnable *routine = new WebServerRoutine(servedSites, clientSocket);
        Thread *thread = new Thread(routine);
        thread->start();
        routines.push_back(std::make_pair(thread, routine));

        ts_printf("Added routine, now: %d\n", routines.size());

        // clear finished routines
        for (auto it = routines.begin(); it != routines.end(); ) {
            if (!(*it).first->join(1)) {
                delete (*it).first;
                delete (*it).second;
                routines.erase(it);
            } else {
                it++;
            }
        }

        return 0;
    }

};


#endif //WIN_API_THREADING_WEBSERVER_H
