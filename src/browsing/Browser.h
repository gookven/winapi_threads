//
// Created by Kv on 11.12.2015.
//

#ifndef WIN_API_THREADING_BROWSER_H
#define WIN_API_THREADING_BROWSER_H


#include "../utils/Service.h"
#include "../utils/OneWayPipe.h"
#include "../utils/TCPSocket.h"

class Browser : public Service {

    OneWayPipe<char> *userToBrowserPipe;
    OneWayPipe<int> *browserToUserPipe;

    OneWayPipe<Request> *browserToWebserverPipe;
    OneWayPipe<Response> *webserverToBrowserPipe;

    OneWayPipe<Response> *browserToParserPipe;
    OneWayPipe<int> *parserToBrowserResources;
    OneWayPipe<ParsedWebpageData> *parserToBrowserData;
    Semaphore *isParsingSemaphore;

public:
    Browser() : Service() {
        initialize();
    }

    ~Browser() {
        delete userToBrowserPipe;
        delete browserToUserPipe;
        delete browserToWebserverPipe;
        delete webserverToBrowserPipe;

        delete browserToParserPipe;
        delete parserToBrowserResources;
        delete parserToBrowserData;
        delete isParsingSemaphore;
    }

protected:

    virtual void initialize() override {
        userToBrowserPipe = new OneWayPipe<char>(PipeName::userToBrowser);
        browserToUserPipe = new OneWayPipe<int>(PipeName::browserToUser);

        browserToWebserverPipe = new OneWayPipe<Request>(PipeName::browserToWebserver);
        webserverToBrowserPipe = new OneWayPipe<Response>(PipeName::webserverToBrowser);

        browserToParserPipe = new OneWayPipe<Response>(PipeName::browserToParser);
        parserToBrowserResources = new OneWayPipe<int>(PipeName::parserToBrowserResources);
        parserToBrowserData = new OneWayPipe<ParsedWebpageData>(PipeName::parserToBrowserData);
        isParsingSemaphore = Semaphore::CreateSemaphore("parser/working");
    }

    DWORD virtual execute() override {

        // the browser is waiting for requests
        Request request;

        try {
            request.site = userToBrowserPipe->read(5000);
        } catch (runtime_error exception) {
            ts_printf(
                    "The browser didn't recieve requests for 5s! \n Error: %s \n",
                    exception.what()
            );

            return 0;
        }

        TCPSocket clientSocket;

        try {
            clientSocket.connect();
        } catch (runtime_error exception) {
            ts_printf(
                    "Creating connection! \n Error: %s \n",
                    exception.what()
            );
        }

        // we have a request!
        ts_printf("Browser is sending request: %c \n", request.site);

        try {
            DataPipe<Request> requestThroughSocketPipe(&clientSocket);
            requestThroughSocketPipe.write(request);
            ts_printf("Request sent!\n");
        } catch (runtime_error exception) {
            ts_printf(
                    "During sending! \n Error: %s \n",
                    exception.what()
            );
            clientSocket.close();

            return 0;
        }

        Response response;

        try {
            DataPipe<Response> responseThroughSocketPipe(&clientSocket);
            response = responseThroughSocketPipe.read();
        } catch (runtime_error exception) {
            ts_printf(
                    "During recieve! \n Error: %s \n",
                    exception.what()
            );
            clientSocket.close();

            return 0;
        }

        clientSocket.close();

        ts_printf("Plain text response to be parsed: \n %s \n", response.plainTextResponse);

        try {
            browserToParserPipe->write(response, 2000);
        } catch (runtime_error exception) {
            ts_printf(
                    "Browser couldn't send data to parser within 2s! \n Error: %s \n",
                    exception.what()
            );

            return 0;
        }

        while (isParsingSemaphore->acquire(100)) {
            // while parser is working - checking if it parsed some resources already
            try {
                int resource = parserToBrowserResources->read(10);
                ts_printf("Parsed resource id: %d! Possibly need to dodwnload!  \n", resource);
            } catch (runtime_error exception) {
                ts_printf("No resources parsed, recheck in 100ms\n");
            }
        }
        isParsingSemaphore->release();

        // make sure nothing is stuck in pipe
        try {
            int resource = parserToBrowserResources->read(10);
            ts_printf("Parsed resource id: %d \n", resource);
        } catch (runtime_error exception) {
            ts_printf("No more resources parsed!\n");
        }

        ParsedWebpageData parsedData;

        try {
            parsedData = parserToBrowserData->read(1);
        } catch (runtime_error exception) {
            ts_printf(
                    "Couldn't get parsed data in 1s! \n Error: %s \n",
                    exception.what()
            );
            return 0;
        }

        ts_printf("The webpage was parsed %s!\n", parsedData.isCorrect ? "correctly" : "incorrectly");

        try {
            ts_printf("Trying to show webpage to user... \n");
            browserToUserPipe->write(response.resultCode, 1000);
        } catch (runtime_error exception) {
            ts_printf(
                    "Some error in browser.user pipe, 1s! \n Error: %s \n",
                    exception.what()
            );
            return 0;
        }

        return 0;
    }
};


#endif //WIN_API_THREADING_BROWSER_H
