//
// Created by Kv on 08.12.2015.
//

#ifndef WIN_API_THREADING_COMMON_H
#define WIN_API_THREADING_COMMON_H

#include <string>

void ts_printf(const char *const format, ...);

using namespace std;

class PipeName {
public:
    static string userToBrowser;
    static string browserToUser;
    static string browserToWebserver;
    static string webserverToBrowser;
    static string browserToParser;
    static string parserToBrowserResources;
    static string parserToBrowserData;
};

struct Request {
    char site;
};

struct Response {
    int resultCode;
    int length;
    char plainTextResponse[128];
};

struct ParsedWebpageData {
    bool isCorrect;

    char linksCount;
    char links[128];

    int resourcesCount;
    int resources[128];
};

const string applicationPrefix = "AppBrowsing/";

const int ExecutionTime = 10000;

#endif //WIN_API_THREADING_COMMON_H
