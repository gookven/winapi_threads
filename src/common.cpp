//
// Created by Kv on 08.12.2015.
//

#include "common.h"
#include <stdarg.h>
#include <stdio.h>
#include "utils/Semaphore.h"

string PipeName::userToBrowser = "user.browser";
string PipeName::browserToUser = "browser.user";
string PipeName::browserToWebserver = "browser.webserver";
string PipeName::webserverToBrowser = "webserver.browser";
string PipeName::browserToParser = "browser.parser";
string PipeName::parserToBrowserResources = "parser.browser(resources)";
string PipeName::parserToBrowserData = "parser.browser(data)";

Semaphore *printfSemaphore = NULL;

void ts_printf(const char *const format, ...) {
    if (printfSemaphore == NULL) {
        printfSemaphore = Semaphore::CreateSemaphore(applicationPrefix + "thread_safe_printf");
    }

    printfSemaphore->acquire();

    va_list args;
    va_start( args, format );
    vprintf( format, args );
    va_end( args );

    printfSemaphore->release();
}